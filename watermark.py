#encoding: utf8

from PIL import Image, ImageDraw, ImageFont
import os





#draw text on image. 
#x,y text position from upper left corner
def draw(filename, text, font_size, x, y):
    
    # Open image
    im = Image.open(os.path.join('images', filename))
     
    # Create new image
    watermark = Image.new("RGBA", im.size)
    font = ImageFont.truetype("UbuntuMono-B.ttf", font_size)
   
    # Get an ImageDraw object so we can draw on the image
    waterdraw = ImageDraw.ImageDraw(watermark, "RGBA")
    
    #shadow
    waterdraw.text((x-1, y-1), text, font=font, fill="white")
    waterdraw.text((x+1, y-1), text, font=font, fill="white")
    waterdraw.text((x-1, y-1), text, font=font, fill="white")
    waterdraw.text((x+1, y+1), text, font=font, fill="white")
    #number
    waterdraw.text((x, y), text, font=font, fill="black")
    
    #save
    im.paste(watermark, None, watermark)
    im.save(os.path.join("images-watermarked", filename), "JPEG")
 
if __name__ == '__main__':
    #get list of images in 'images' folder
    images = [os.path.join('images', fn) for fn in os.listdir('images') if os.path.isfile(os.path.join('images', fn))]
  
    #text is number
    iter = 1
    for image in images:
        #get image filename without folder path
        filename = image.split('/')[1]
        #skip dotfiles eg .DS_Store
        if filename.startswith('.'):
            continue
        #draw functions arguments determine how text will be added
        draw(filename=filename, text=str(iter), font_size=35, x=1, y=1)
        iter += 1
        